# PAYFAZZ CODING CHALLENGE

- Web scraper
- API
- Unit Tests

### Installation
Install the dependencies and devDependencies and start the server.

```sh
$ npm install
$ npm start
```

For unit tests environments...

```sh
$ npm test
$ npm cover
```

### .env
```sh
DB=mongodb+srv://febriliando:password0702@cluster0-hzkl2.mongodb.net/test?retryWrites=true
```

### Link Postman
```sh
https://www.getpostman.com/collections/27da5d04750508496e23
```
