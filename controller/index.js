const moment = require('moment');

const Currency = require('../models');
const { baseScraper } = require('../repositories/scrapers');

const baseURL =
    'https://www.bca.co.id/id/Individu/Sarana/Kurs-dan-Suku-Bunga/Kurs-dan-Kalkulator';

exports.scraper = async (req, res) => {
    try {
        const rowData = await baseScraper(baseURL);
        const today = moment().startOf('day');
        const existData = await Currency.find({
            date: {
                $gte: today.toDate(),
                $lte: moment(today)
                    .endOf('day')
                    .toDate()
            }
        });
        if (existData.length > 0) {
            res.status(200).json({
                message: 'data already exists'
            });
        } else {
            Currency.insertMany(rowData)
                .then(() => {
                    res.status(200).json({
                        message: 'success scrapping and insert data',
                        data: rowData
                    });
                })
                .catch(() => {
                    res.status(500).json({
                        message: 'Failed Insert data'
                    });
                });
        }
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.getCurrencyByDate = async (req, res) => {
    try {
        const date = moment(req.query.startdate);
        const maxDate = moment(req.query.enddate).endOf("day").utc();

        const rowData = await Currency.find({
            $and: [
                {
                    date: {
                        $lte: new Date(maxDate),
                        $gte: new Date(date)
                    }
                }
            ]
        });

        if (!rowData.length) {
            res.status(404).json({
                message: 'Data not found'
            });
        } else {
            res.status(200).json({
                message: 'success retreived data',
                data: rowData
            });
        }
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.getCurrencyBySymbol = async (req, res) => {
    try {
        const { query, params } = req;
        const buildQuery = {
            symbol: params.symbol
        };
        if (query.startdate) {
            buildQuery.date = {
                $gte: query.startdate
            };
        }
        if (query.enddate) {
            buildQuery.date = {
                ...buildQuery.date,
                $lte: query.enddate
            };
        }
        const rowData = await Currency.find(buildQuery);
        res.status(200).json({
            message: 'success retreived data',
            data: rowData
        });
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.removeById = async (req, res) => {
    try {
        const { params } = req;
        const date = moment(params.date).utc();
        const maxDate = moment(date).endOf("day");
        const removed = await Currency.deleteMany({
            $and: [
                {
                    date: {
                        $lte: new Date(maxDate),
                        $gte: new Date(date)
                    }
                }
            ]
        });
        res.status(200).json({
            message: 'success removed',
            removed
        });
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.postCurency = async (req, res) => {
    try {
        const { body } = req;
        const maxDate = moment(moment(body.date).format('YYYY-MM-DD'),'YYYY-MM-DD').add('YYYY-MM-DD', 1);
        const dataExists = await Currency.find({
            symbol: body.symbol,
            $and: [
                {
                    date: {
                        $lte: body.date,
                        $gte: maxDate
                    }
                }
            ]
        });
        if (dataExists.length > 0) {
            res.status(200).json({
                message: 'data already exists'
            });
        } else {
            const result = await Currency.create(body);
            res.status(200).json({
                message: 'success post curency',
                data: result
            });
        }
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.updateCurency = async (req, res) => {
    try {
        const { body } = req;
        const maxDate = moment(moment(body.date).format('YYYY-MM-DD'),'YYYY-MM-DD').add('YYYY-MM-DD', 1);
        const dataExists = await Currency.findOne({
            symbol: body.symbol,
            $and: [
                {
                    date: {
                        $lte: body.date,
                        $gte: maxDate
                    }
                }
            ]
        });

        if (!dataExists) {
            res.status(404).json({
                message: 'data not found'
            })
        } else {
            const dataToUpdate = JSON.parse(JSON.stringify(body, null, 4));
            Currency.findOneAndUpdate({_id: dataExists._id}, dataToUpdate).then((updated) => {
                res.status(200).json({
                    message: 'success update',
                    data: dataToUpdate
                });
            })
        }
    } catch (err) {
        res.status(500).send(err)
    }
}

module.exports = exports;
