const Joi = require('joi');

const schema = {
    vatidatorGetByDate: Joi.object().keys({
        startdate: Joi.date().required(),
        enddate: Joi.date().required()
    }),
    vatidatorGetBySymbol: Joi.object().keys({
        params: Joi.object().keys({
            symbol: Joi.string().alphanum().min(3).max(10).required()
        }),
        query: Joi.object().keys({
            startdate: Joi.date().allow(''),
            enddate: Joi.date().allow('')
        })
    }),
    vatidatorRemoveById: Joi.object().keys({
        date: Joi.date().required()
    }),
    validatorPostCurency: Joi.object().keys({
        symbol: Joi.string().alphanum().min(3).max(10).required(),
        e_rate: Joi.object().keys({
            jual: Joi.string().required(),
            beli: Joi.string().required()
        }),
        tt_counter: Joi.object().keys({
            jual: Joi.string().required(),
            beli: Joi.string().required()
        }),
        bank_notes: Joi.object().keys({
            jual: Joi.string().required(),
            beli: Joi.string().required()
        }),
        date: Joi.date().required()
    })
};

module.exports = {
    vatidatorGetByDate: async (req, res, next) => {
        const { query } = req;
        const result = Joi.validate(query, schema['vatidatorGetByDate']);
        if (result.error !== null) {
            let message = result.error.message;
            message = message.replace(/"/g, '');
            res.status(400).json({ message: message });
        } else {
            req.query = result.value
            next();
        }
    },
    vatidatorGetBySymbol: async (req, res, next) => {
        const { query, params } = req;

        const result = Joi.validate({ params, query }, schema['vatidatorGetBySymbol']);
        if (result.error !== null) {
            let message = result.error.message;
            message = message.replace(/"/g, '');
            res.status(400).json({ message: message });
        } else {
            req.params = result.value.params;
            req.query = result.value.query;
            next();
        }
    },
    vatidatorRemoveById: async (req, res, next) => {
        const result = Joi.validate( req.params, schema['vatidatorRemoveById']);
        if (result.error !== null) {
            let message = result.error.message;
            message = message.replace(/"/g, '');
            res.status(400).json({ message: message });
        } else {
            req.params = result.value;
            next();
        }
    },
    vatidatorPostCurency: async (req, res, next) => {
        const { body } = req;
        const result = Joi.validate(body, schema['validatorPostCurency']);
        if (result.error !== null) {
            let message = result.error.message;
            message = message.replace(/"/g, '');
            res.status(400).json({ message: message });
        } else {
            req.body = result.value;
            next();
        }
    },
    validatorUpdateCurency: async (req, res, next) => {
        const { body } = req;
        const result = Joi.validate(body, schema['validatorPostCurency']);
        if (result.error !== null) {
            let message = result.error.message;
            message = message.replace(/"/g, '');
            res.status(400).json({ message: message });
        } else {
            req.body = result.value;
            next();
        }
    }
};

