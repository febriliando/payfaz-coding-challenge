const request = require('request');

module.exports = {
    request: url => {
        return new Promise((resolves, rejects) => {
            request(url, (error, response, html) => {
                if (!error && response.statusCode === 200) {
                    resolves({
                        response,
                        html
                    });
                }
            });
        });
    }
};
