const app = require('./app.js');
const http = require('http');
const server = http.createServer(app);
const PORT = process.env.NODE_ENV || 7000;

server.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
});
