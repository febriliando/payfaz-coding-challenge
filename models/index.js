const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const currencySchema = new Schema({
    symbol: String,
    e_rate: {
        jual: String,
        beli: String
    },
    tt_counter: {
        jual: String,
        beli: String
    },
    bank_notes: {
        jual: String,
        beli: String
    },
    date: { type: Date, default: new Date() },
    createdAt  : { type: Date, default: new Date() }
  })
  const Currency = mongoose.model('currency', currencySchema)
  
  module.exports = Currency;
