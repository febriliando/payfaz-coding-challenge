const Promise = require('bluebird');
const sinon = require('sinon');
const test = require('ava');
const request = require('supertest');

const app = require('../app');
const CurrencyModel = require('../models');
const Method = require('../controller');
const mockup = [
    {
        e_rate: {
            jual: '14.136,00',
            beli: '14.120,00'
        },
        tt_counter: {
            jual: '14.278,00',
            beli: '13.978,00'
        },
        bank_notes: {
            jual: '14.325,00',
            beli: '14.025,00'
        },
        date: '2019-04-05T00:00:00.000Z',
        createdAt: '2019-04-05T14:18:19.993Z',
        _id: '5ca763af96464841c5113a13',
        symbol: 'USD',
        __v: 0
    }
];

test.serial('it should Success getCurrencyBySymbol', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'find').resolves(mockup)
    const response = await request(app).get('/api/kurs/USD?startdate=2019-04-05&enddate=2019-04-05');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success retreived data',
        data: mockup
    });
});

test.serial('it should Success without startdate getCurrencyBySymbol', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'find').resolves(mockup)
    const response = await request(app).get('/api/kurs/USD?enddate=2019-04-05');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success retreived data',
        data: mockup
    });
});

test.serial('it should Success without enddate getCurrencyBySymbol', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'find').resolves(mockup)
    const response = await request(app).get('/api/kurs/USD?startdate=2019-04-05');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success retreived data',
        data: mockup
    });
});

test.serial('it should throw Error getCurrencyBySymbol', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'find').rejects(new Error('Error Connection Database'))
    try {
        const response = await request(app).get('/api/kurs/USD?startdate=2019-04-05');
        t.is(response.status, 500);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.serial('it should throw error bad request, getCurrencyByDate', async (t) => {
    try {
        const response = await request(app).get(
            '/api/kurs/USD?startdaate=2019-04-05&enddate=2019-04-05'
        );
        t.is(response.status, 400);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.beforeEach('Initialize New Sandbox Before Each Test', async t => {
    t.context.sandbox = sinon.createSandbox().usingPromise(Promise.Promise);
});

test.afterEach.always(
    'Restore Sandbox and Configuration After Each Test',
    async t => {
        t.context.sandbox.restore();
    }
);
