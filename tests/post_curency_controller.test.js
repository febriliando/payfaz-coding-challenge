const Promise = require('bluebird');
const sinon = require('sinon');
const test = require('ava');
const request = require('supertest');

const app = require('../app');
const CurrencyModel = require('../models');

const mockup = [
    {
        symbol: 'USD',
        e_rate: {
            jual: '14.136,00',
            beli: '14.120,00'
        },
        tt_counter: {
            jual: '14.278,00',
            beli: '13.978,00'
        },
        bank_notes: {
            jual: '14.325,00',
            beli: '14.025,00'
        },
        date: '2019-04-05'
    }
];
const mockupSave = {
    date: '2019-04-05T00:00:00.000Z',
    createdAt: '2019-04-05T15:52:23.985Z',
    _id: '5ca779d143afda55d4493641',
    symbol: 'IDR',
    e_rate: {
        jual: '1803.55',
        beli: '1773.55'
    },
    tt_counter: {
        jual: '1803.55',
        beli: '1773.55'
    },
    bank_notes: {
        jual: '1803.55',
        beli: '1773.55'
    },
    __v: 0
};

test.serial('it should Success but bot save postCurency', async t => {
    t.context.sandbox.stub(CurrencyModel, 'find').resolves(mockup);
    const response = await request(app)
        .post('/api/kurs')
        .send({
            symbol: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'data already exists'
    });
});

test.serial('it should Success postCurency', async t => {
    t.context.sandbox.stub(CurrencyModel, 'find').resolves([]);
    t.context.sandbox.stub(CurrencyModel, 'create').resolves(mockupSave);
    const response = await request(app)
        .post('/api/kurs')
        .send({
            symbol: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success post curency',
        data: mockupSave
    });
});

test.serial('it should throw error postCurency', async t => {
    t.context.sandbox.stub(CurrencyModel, 'find').rejects(new Error('Error Connection Database'));
    try {
        const response = await request(app)
            .post('/api/kurs')
            .send({
                symbol: 'IDR',
                e_rate: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                tt_counter: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                bank_notes: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                date: '2019-04-05'
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/);
        t.is(response.status, 500);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.serial('it should throw bad request, postCurency', async t => {
    try {
        const response = await request(app)
        .post('/api/kurs')
        .send({
            symbola: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
        t.is(response.status, 400);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.beforeEach('Initialize New Sandbox Before Each Test', async t => {
    t.context.sandbox = sinon.createSandbox().usingPromise(Promise.Promise);
});

test.afterEach.always('Restore Sandbox and Configuration After Each Test', async t => {
        t.context.sandbox.restore();
});
