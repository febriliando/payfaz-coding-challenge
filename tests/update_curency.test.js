const Promise = require('bluebird');
const sinon = require('sinon');
const test = require('ava');
const request = require('supertest');

const app = require('../app');
const CurrencyModel = require('../models');
const mockup = {  
    e_rate:{  
        jual:'1303.55',
        beli:'1773.55'
    },
    tt_counter:{  
        jual:'1803.55',
        beli:'1773.55'
    },
    bank_notes:{  
        jual:'2200.55',
        beli:'3100.55'
    },
    date: '2019-04-05T00:00:00.000Z',
    createdAt: '2019-04-05T15:52:23.985Z',
    symbol:'IDR',
}

test.serial('it should throw error not found updateCurency', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'findOne').resolves(null);
    const response = await request(app)
        .put('/api/kurs')
        .send({
            symbol: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
    t.is(response.status, 404);
    t.deepEqual(response.body, {
        message: 'data not found'
    });
});

test.serial('it should throw internalserver error  updateCurency', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'findOne').rejects(new Error('Error Connection Database'));
    try {
        const response = await request(app)
            .put('/api/kurs')
            .send({
                symbol: 'IDR',
                e_rate: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                tt_counter: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                bank_notes: {
                    jual: '1803.55',
                    beli: '1773.55'
                },
                date: '2019-04-05'
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/);
        t.is(response.status, 500);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.serial('it should Success updateCurency', async (t) => {
    t.context.sandbox.stub(CurrencyModel, 'findOne').resolves(mockup);
    t.context.sandbox.stub(CurrencyModel, 'findOneAndUpdate').resolves();
    const response = await request(app)
        .put('/api/kurs')
        .send({
            symbol: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success update',
        data: {
            symbol: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05T00:00:00.000Z'
        }
    });
});

test.serial('it should throw bad request, updateCurency', async t => {
    try {
        const response = await request(app)
        .put('/api/kurs')
        .send({
            symbola: 'IDR',
            e_rate: {
                jual: '1803.55',
                beli: '1773.55'
            },
            tt_counter: {
                jual: '1803.55',
                beli: '1773.55'
            },
            bank_notes: {
                jual: '1803.55',
                beli: '1773.55'
            },
            date: '2019-04-05'
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/);
        t.is(response.status, 400);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.beforeEach('Initialize New Sandbox Before Each Test', async (t) => {
    t.context.sandbox = sinon.createSandbox().usingPromise(Promise.Promise);
});

test.afterEach.always('Restore Sandbox and Configuration After Each Test', async (t) => {
    t.context.sandbox.restore();    
});