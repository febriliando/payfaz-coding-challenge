const Promise = require('bluebird');
const sinon = require('sinon');
const test = require('ava');
const request = require('supertest');

const app = require('../app');
const CurrencyModel = require('../models');
const mockup = {
    n: 16,
    opTime: {
        ts: '6676397993001746449',
        t: 1
    },
    electionId: '7fffffff0000000000000001',
    ok: 1,
    operationTime: '6676397993001746449',
    $clusterTime: {
        clusterTime: '6676397993001746449',
        signature: {
            hash: 'HNvyGPTX3SupqT9ztYBz/4rSqnA=',
            keyId: '6675793914441498625'
        }
    },
    deletedCount: 16
};

test.serial('it should Success removeById', async t => {
    t.context.sandbox.stub(CurrencyModel, 'deleteMany').resolves(mockup);
    const response = await request(app).delete('/api/kurs/2019-04-05');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success removed',
        removed: mockup
    });
});

test.serial('it should throw internalserver error removeById', async t => {
    t.context.sandbox.stub(CurrencyModel, 'deleteMany').rejects(new Error('Error Connection Database'));
    try {
        const response = await request(app).delete('/api/kurs/2019-04-05');
        t.is(response.status, 500);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.serial('it should throw bad request, removeById', async t => {
    try {
        const response = await request(app).delete('/api/kurs/2019-13-32');
        t.is(response.status, 400);
    } catch (err) {
        t.fail('shouldnt throw error');
    }
});

test.beforeEach('Initialize New Sandbox Before Each Test', async t => {
    t.context.sandbox = sinon.createSandbox().usingPromise(Promise.Promise);
});

test.afterEach.always(
    'Restore Sandbox and Configuration After Each Test',
    async t => {
        t.context.sandbox.restore();
    }
);
