const Promise = require('bluebird');
const sinon = require('sinon');
const test = require('ava');
const request = require('supertest');
const moment = require('moment');

const app = require('../app');
const baseScraper = require('../repositories/scrapers');
const CurrencyModel = require('../models');

const mockup = [
    {
        symbol: 'USD',
        e_rate: {
            jual: '14.136,00',
            beli: '14.120,00'
        },
        tt_counter: {
            jual: '14.278,00',
            beli: '13.978,00'
        },
        bank_notes: {
            jual: '14.325,00',
            beli: '14.025,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'SGD',
        e_rate: {
            jual: '10.457,88',
            beli: '10.409,88'
        },
        tt_counter: {
            jual: '10.474,70',
            beli: '10.392,70'
        },
        bank_notes: {
            jual: '10.579,00',
            beli: '10.349,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'EUR',
        e_rate: {
            jual: '15.916,45',
            beli: '15.816,45'
        },
        tt_counter: {
            jual: '16.061,45',
            beli: '15.681,45'
        },
        bank_notes: {
            jual: '16.117,00',
            beli: '15.703,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'AUD',
        e_rate: {
            jual: '10.106,91',
            beli: '10.026,91'
        },
        tt_counter: {
            jual: '10.200,50',
            beli: '9.929,50'
        },
        bank_notes: {
            jual: '10.228,00',
            beli: '9.950,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'DKK',
        e_rate: {
            jual: '2.155,62',
            beli: '2.095,62'
        },
        tt_counter: {
            jual: '2.163,80',
            beli: '2.097,00'
        },
        bank_notes: {
            jual: '2.193,00',
            beli: '2.063,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'SEK',
        e_rate: {
            jual: '1.542,31',
            beli: '1.502,31'
        },
        tt_counter: {
            jual: '1.551,20',
            beli: '1.497,80'
        },
        bank_notes: {
            jual: '1.585,00',
            beli: '1.467,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'CAD',
        e_rate: {
            jual: '10.619,61',
            beli: '10.539,61'
        },
        tt_counter: {
            jual: '10.706,35',
            beli: '10.440,35'
        },
        bank_notes: {
            jual: '10.750,00',
            beli: '10.450,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'CHF',
        e_rate: {
            jual: '14.178,71',
            beli: '14.078,71'
        },
        tt_counter: {
            jual: '14.305,15',
            beli: '13.962,15'
        },
        bank_notes: {
            jual: '14.365,00',
            beli: '13.980,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'NZD',
        e_rate: {
            jual: '9.562,28',
            beli: '9.482,28'
        },
        tt_counter: {
            jual: '9.664,65',
            beli: '9.377,65'
        },
        bank_notes: {
            jual: '9.712,00',
            beli: '9.440,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'GBP',
        e_rate: {
            jual: '18.532,96',
            beli: '18.432,96'
        },
        tt_counter: {
            jual: '18.723,10',
            beli: '18.245,10'
        },
        bank_notes: {
            jual: '18.773,00',
            beli: '18.311,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'HKD',
        e_rate: {
            jual: '1.814,90',
            beli: '1.784,90'
        },
        tt_counter: {
            jual: '1.816,90',
            beli: '1.783,00'
        },
        bank_notes: {
            jual: '1.840,00',
            beli: '1.771,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'JPY',
        e_rate: {
            jual: '128,19',
            beli: '124,79'
        },
        tt_counter: {
            jual: '128,88',
            beli: '124,15'
        },
        bank_notes: {
            jual: '130,23',
            beli: '123,68'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'SAR',
        e_rate: {
            jual: '3.806,99',
            beli: '3.726,99'
        },
        tt_counter: {
            jual: '3.819,75',
            beli: '3.714,75'
        },
        bank_notes: {
            jual: '3.858,00',
            beli: '3.689,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'CNY',
        e_rate: {
            jual: '2.166,01',
            beli: '2.046,02'
        },
        tt_counter: {
            jual: '2.190,15',
            beli: '2.023,85'
        },
        bank_notes: {
            jual: '2.172,00',
            beli: '2.043,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'MYR',
        e_rate: {
            jual: '3.496,40',
            beli: '3.416,40'
        },
        tt_counter: {
            jual: '3.505,80',
            beli: '3.403,80'
        },
        bank_notes: {
            jual: '0,00',
            beli: '0,00'
        },
        date: moment().format('YYYY-MM-DD')
    },
    {
        symbol: 'THB',
        e_rate: {
            jual: '447,03',
            beli: '439,03'
        },
        tt_counter: {
            jual: '447,95',
            beli: '437,95'
        },
        bank_notes: {
            jual: '455,00',
            beli: '426,00'
        },
        date: moment().format('YYYY-MM-DD')
    }
];

test.serial('Success scrapping and save data', async t => {
    t.context.sandbox.stub(baseScraper, 'baseScraper').resolves(mockup);
    t.context.sandbox.stub(CurrencyModel, 'find').resolves([]);
    t.context.sandbox.stub(CurrencyModel, 'insertMany').resolves(mockup);
    const response = await request(app).get('/api/indexing');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'success scrapping and insert data',
        data: mockup
    });
});

test.serial('Success scrapping and not save data, existing', async t => {
    t.context.sandbox.stub(baseScraper, 'baseScraper').resolves(mockup);
    t.context.sandbox.stub(CurrencyModel, 'find').resolves(mockup);
    const response = await request(app).get('/api/indexing');
    t.is(response.status, 200);
    t.deepEqual(response.body, {
        message: 'data already exists'
    });
});

test.serial('Error when saving', async t => {
    t.context.sandbox.stub(baseScraper, 'baseScraper').resolves(mockup);
    t.context.sandbox.stub(CurrencyModel, 'find').resolves([]);
    t.context.sandbox.stub(CurrencyModel, 'insertMany').rejects(new Error());
    const response = await request(app).get('/api/indexing');
    t.is(response.status, 500);
    t.deepEqual(response.body, {
        message: 'Failed Insert data'
    });
});

test.serial('Error when scraping', async t => {
    t.context.sandbox.stub(baseScraper, 'baseScraper').resolves(mockup);
    t.context.sandbox.stub(CurrencyModel, 'find').rejects(new Error());
    const response = await request(app).get('/api/indexing');
    t.is(response.status, 500);

});

test.beforeEach('Initialize New Sandbox Before Each Test', async (t) => {
    t.context.sandbox = sinon.createSandbox().usingPromise(Promise.Promise);
});

test.afterEach.always('Restore Sandbox and Configuration After Each Test', async (t) => {
    t.context.sandbox.restore();    
});
