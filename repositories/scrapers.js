const cherio = require('cherio');
const moment = require('moment');

const { request } = require('../helpers/request');

const hasNumber = myString => {
    return /\d/.test(myString);
};

module.exports = {
    baseScraper: async url => {
        const { html } = await request(url);
        const arr = [];
        const $ = cherio.load(html, { normalizeWhitespace: true });
        $('.text-right').map((i, el) => {
            const items = $(el)
                .text()
                .trim()
                .replace(/\s\s+/g, ' ')
                .split(' ');
            items.map((r, i) => {
                if (!hasNumber(r)) {
                    arr.push({
                        symbol: r,
                        e_rate: {
                            jual: items[i + 1],
                            beli: items[i + 2]
                        },
                        tt_counter: {
                            jual: items[i + 3],
                            beli: items[i + 4]
                        },
                        bank_notes: {
                            jual: items[i + 5],
                            beli: items[i + 6]
                        },
                        // moment(moment().format('YYYY-MM-DD'), "YYYY-MM-DD").add("days", 4)
                        date: moment().format('YYYY-MM-DD')
                    });
                }
            });
        });
        return arr;
    }
};
