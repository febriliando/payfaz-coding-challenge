const router = require('express').Router();

const {
    scraper,
    getCurrencyByDate,
    getCurrencyBySymbol,
    removeById,
    postCurency,
    updateCurency
} = require('../controller');

const {
    vatidatorGetByDate,
    vatidatorGetBySymbol,
    vatidatorRemoveById,
    vatidatorPostCurency,
    validatorUpdateCurency
} = require('../middleware/validator');

router.get('/indexing', scraper);
router.get('/kurs', vatidatorGetByDate, getCurrencyByDate);
router.post('/kurs', vatidatorPostCurency, postCurency);
router.put('/kurs', validatorUpdateCurency, updateCurency);
router.get('/kurs/:symbol', vatidatorGetBySymbol, getCurrencyBySymbol);
router.delete('/kurs/:date', vatidatorRemoveById, removeById);

module.exports = router;
